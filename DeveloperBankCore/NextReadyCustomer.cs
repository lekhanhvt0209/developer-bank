﻿public class NextReadyCustomer
{
    public Customer[] VIPCustomers { get; set; }
    public Customer[] EconomyCustomers { get; set; }

    public int VIPQueueCount { get; set; }
    public int EconomyQueueCount { get; set; }
}
